# -*- coding: utf-8 -*-

fwd_name=$1
cfg_name=$2
n_cpu=$3
thread_id=$4
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1

if [[ $thread_id == "0" ]]; then
	echo "================================================================================"
    echo "Starting custEM forward / Jacobian calculations via bash scripts"
fi

mpirun -n "$n_cpu" python -u "$fwd_name" "$cfg_name" "$thread_id"
