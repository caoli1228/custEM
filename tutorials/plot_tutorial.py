#!/usr/bin/env python
# coding: utf-8

# # Visualization tutorial
#  
# This plot tutorial introduces basic functions for importing interpolated data
# from CSEM or MT computations and depicting them latter in 1D, 2D or 3D.
# This tutorial is going to be extended to include even more useful commands for
# analyzing different models computed with custEM (mismatches, field variations)
# or compare ustEM models with external results for validation/cross-checking.
# Meanwhile, we refer to the API-documentation for many more options.

# # Import modules

# In[ ]:


from custEM.post import make_plain_subfigure_box
from custEM.post import PlotFD as Plot
import matplotlib.pyplot as plt


# # Initialize Plot instance and import results

# In[ ]:


# initialize Plot instance
# #############################################################################

P = Plot('test_mod',                  # model name
         'test_mesh',                 # mesh name
         'E_t',                       # approach name
         # r_dir='results',           # results directory
         # s_dir='plots',             # save directory for default plots
         fig_size=[16, 12]            # change figure size
         )
         
line1 = 'surf_line_x'
slice1 = 'air_slice_z'

# If not overwritten by keywords, data will be always imported from the
# model specified before in the *Plot* class initialization. Overwriting
# the keyword arguemnts during the import allows adding furhter data from 
# other models, meshes, or approaches for comparison puposes.
         
# Note that *tx* and *freq* keyword arguments only need to be specified in
# case of multiple Tx or Frequencies in the same model, otherwise results 
# are only avaialble for one Tx or freq and identified automatically.

for ti in range(2):
    P.import_line_data(line1,                # line mesh name 
                       # mod='mod_name',     # specify alternative mod name
                       # mesh='mesh_name',   # specify alternative mesh name
                       # mpproach='E_s',     # specify alternative approach
                       tx=ti,                # specify which Tx
                       freq=1,               # load data of second freq 100 Hz
                       key='L' + str(ti)     # give key to access data later on
                       )   
         
    P.import_slice_data(slice1,              # line mesh name 
                        tx=ti,               # specify which Tx
                        freq=1,              # load data of second freq 100 Hz
                        # stride=1,          # downsample data to speed up plot
                        key='S' + str(ti)    # give key to access data later on
                        )        


# # Plot line and slice data

# In[ ]:


# plot data and misfits
# #############################################################################

# if a title is specified, plots are saved in the *p_dir*, default 'plots'

# initialize a plot with all three components for E and H fields
P.plot_line_data(key='L0', title='Tx0')

# add further line data to the same plot
P.plot_line_data(key='L1', new=False, title='Tx1')

# make a more customizedline plot with amplitude and phase representation
# use custom coordinate and amplitude limits
# use specific matplotlib style keyword arguments
P.plot_line_data(key='L0', ap=True,
                 xlim=[-1., 1.], ylim=[1e-7, 1e-2],
                 EH='E', color='k', lw='0.5', ls='-',
                 marker='^', markersize='3', title='Tx0_amp_phase')

# plot slice data in the air
P.plot_slice_data(key='S1', EH='H', title='Tx0_air')

# plot misfits, rather nonsense to compare data from two different Tx
# in this case, but shows the functionality
P.plot_line_errors(key='L0', key2='L1', EH='E')
P.plot_slice_errors(key='S0', key2='S1', EH='H')


# # Plot 2D data in 3D

# In[ ]:


# Will be updated soon with a proper example
# #############################################################################

# fig = plt.figure(figsize=(10, 7))
# P.add_3D_slice_plot(fig, 'K1_H_t', 'above_topo_slice_z', sp_pos=111,
#                     q_length=0.2, label=r'$\Re(\mathbf{|H|})$ $(V/ m)$',
#                     q_slicE='K2', q_name='K2_H_t', clim=[1e-8, 1e-4])
# plt.savefig('./plots/custom/H_r_3D_plot.png')
#
# fig = plt.figure(figsize=(10, 7))
# P.add_3D_slice_plot(fig, 'K1_H_t', 'above_topo_slice_z', sp_pos=111,
#                     q_length=0.2, label=r'$\Im(\mathbf{|H|})$ $(V/ m)$',
#                     q_slicE='K2', q_name='K2_H_t', clim=[1e-8, 1e-4],
#                     ri='imag')
# plt.savefig('./plots/custom/H_i_3D_plot.png')

