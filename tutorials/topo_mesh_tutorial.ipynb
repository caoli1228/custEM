{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Mesh generation with topography data\n",
    "\n",
    "This tutorial is a guide to create topography meshes for calculations\n",
    "whit custEM. It is possible to either describe a synthetic topography as \n",
    "f(x, y) or to incorporate real DEM (digital elevation model) data.\n",
    "Synthetic topographies can be definied in *the synthetic_definitions.py*\n",
    "or a custom file or directly in this script as will be explained following.\n",
    "\n",
    "As real DEM data files are quite large, we present the usage and structure\n",
    "by exporting synthetic topography data to both supported file formats\n",
    "and use these data to simulate real-world topography interpolation.\n",
    "\n",
    "In the following, we will handle two seperate cases, named **I** and **II**,\n",
    "to show different options or possibilites."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### General information for DEM files\n",
    "\n",
    "- supported file formats are either *xyz* or *asc* files\n",
    "- for now, the DEM data points must be located on a regular grid\n",
    "- the files must be located in the topography directory **t_dir**\n",
    "- *xyz* files contain x-, y-, z- values for each single data point.\n",
    "  The positions must be sorted either **ascending** in x-y coordinate\n",
    "  order or vice versy, in y-x order\n",
    "- *asc* files contain a header with lower left corner and grid spacing,\n",
    "  followed by a matrix of z-values arranged corresponding to the grid\n",
    "- for more information, generate the example files as coded below and\n",
    "  have look at the them"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Import modules, utility functions or synthetic definitions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "from custEM.meshgen import meshgen_utils as mu\n",
    "from custEM.meshgen.meshgen_tools import BlankWorld\n",
    "import numpy as np\n",
    "\n",
    "# import synthetic example-topography description, here valley.\n",
    "# #############################################################################\n",
    "from custEM.misc.synthetic_definitions import valley\n",
    "\n",
    "t_dir = '.'                     # directory for topography files "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### case I - Create example *xyz* file \n",
    "\n",
    "Here we use the *valley* function from *synthetic_definitions*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v = np.linspace(0., 2e4, 801)   # vector with 25 m spacing\n",
    "xy = np.zeros((len(v)**2, 2))   # empty 2D array for all nodes in grid\n",
    "a = 0\n",
    "for j in range(len(v)):         # assign coordinates for rows\n",
    "    for k in range(len(v)):     # assign coordinates for columns\n",
    "        xy[a, 0] = v[j]\n",
    "        xy[a, 1] = v[k]\n",
    "        a += 1\n",
    "\n",
    "# use imported synthetic topo-function to calculate z-values\n",
    "z_data = valley(xy[:, 0], xy[:, 1])  \n",
    "valley_data = np.hstack((xy, z_data.reshape(-1, 1)))\n",
    "\n",
    "# write xyz file\n",
    "mu.write_synth_topo_to_xyz(t_dir, 'valley_DEM', valley_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### case II - Create example *asc* file \n",
    "\n",
    "for this example, we will use a locally definied synthetic function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define topography function\n",
    "# #############################################################################\n",
    "np.random.seed(42)  # set seed for reproducibility\n",
    "\n",
    "def slope1(x, y, h=300., z=200.):\n",
    "    return(1e-4 * h * x - 1e-4 * h * y + z)\n",
    "\n",
    "v = np.linspace(-2e4, 2e4, 401)  # vector with 100 m spacing\n",
    "xy = np.zeros((len(v)**2, 2))    # empty 2D array for all nodes in grid\n",
    "a = 0\n",
    "for j in range(len(v)):          # assign coordinates for rows\n",
    "    for k in range(len(v)):      # assign coordinates for columns\n",
    "        xy[a, 0] = v[j]\n",
    "        xy[a, 1] = v[k]\n",
    "        a += 1\n",
    "\n",
    "z_data = slope1(xy[:, 0], xy[:, 1])           # some basic slope\n",
    "z_data += np.random.rand(len(z_data)) * 100.  # add the \"hills\"\n",
    "\n",
    "# write asc file and assume we have some UTM 32 N coordinates, that's why\n",
    "# we shift our just generated 40 x 40 km outcrop of topo data with help of\n",
    "# *x_min* and *y_min* to the assumed \"real\" coordinate world\n",
    "mu.write_synth_topo_to_asc(t_dir,\n",
    "                           'hilly_DEM',\n",
    "                           z_data.reshape(len(v), len(v)),\n",
    "                           spacing=100.,      # header infor\n",
    "                           x_min=703000,      # lower left x coordinate\n",
    "                           y_min=5602000)     # lower left y coordinate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create computational domains for cases I and II\n",
    "\n",
    "#### Incorporating topography\n",
    "\n",
    "It is possible to define topography (otherwiese topo=None) either with\n",
    "functions or DEM files. The toolbox will automatically calculate z-values\n",
    "for corresponding nodes if a function is used or interpolate\n",
    "z-values from the DEM using the scipy *regulargridinterpolator* on the fly.\n",
    "\n",
    "The examples aim to show the usage of the following important arguments:\n",
    "\n",
    "1. centering: used to shift the computational domain Omega relative to the \n",
    "   center of the coordinate frame. This is in particular useful to shift, for\n",
    "   instance, a 20 x 20 km outcrop of DEM data to the modeling domain.\n",
    "   \n",
    "2. easting_shift & northing_shift: shift Omega relative to the extend of the\n",
    "   DEM in x- and y- direction by these values. This can be used, for instance,\n",
    "   to shift a start- or end-point of the transmitter in the model domain to \n",
    "   the coordiantes measured in the field and hence, simplify the models setup.\n",
    "   \n",
    "3. rotation: rotate clockwise, starting with positive x-axis direction, the \n",
    "   coordinate frame in the field relative to the model domain. This is useful\n",
    "   if a survey setup with parallel and perpedicular lines should correspond\n",
    "   to x- and y-axes in the model domain"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Incorporate topography with *asc* file\n",
    "# #############################################################################\n",
    "\n",
    "M1 = BlankWorld(                     # create blank 3D domain Omega\n",
    "     name='hilly_mesh',              # name of the output mesh\n",
    "     m_dir='./meshes',               # main mesh directory\n",
    "     t_dir=t_dir,                    # topo dir., **p_dir**+ '/topo'\n",
    "     # x_dim=[-1e4, 1e4],            # x-dimension [m] of the mesh\n",
    "     # y_dim=[-1e4, 1e4],            # y-dimension [m] of the mesh\n",
    "     # y_dim=[-1e4, 1e4],            # z-dimension [m] of the mesh\n",
    "     backup_script = False,          # store mesh generation script as backup\n",
    "     #\n",
    "     # keyword arguments regarding building the 2D surface mesh.\n",
    "     #\n",
    "     inner_area='box',           # refinement of area at surface\n",
    "     inner_area_size=[2e3, 1e3],     # size of this area\n",
    "     # inner_area_shift=[0., 0.],    # shift in x- and y-dir of area\n",
    "     inner_area_cell_size=1e3,       # max. triangle-size (MA) in area\n",
    "     outer_area_cell_size=1e5,       # MA outside of this area\n",
    "\n",
    "     # keyword arguments regarding incorporation of topography\n",
    "     #\n",
    "     topo='hilly_DEM.asc',           # topography (from DEM or synthetic),\n",
    "     # # # # # # # # # # # # # # # # #                      e.g. topo=valley\n",
    "     # subsurface_topos=None,        # subsurface interface topo (DEM, synth.)\n",
    "     easting_shift=-723000,          # shift DEM east. relative to model domain\n",
    "     northing_shift=-5622000,        # shift DEM north. relative to model domain\n",
    "     # centering=False,              # center DEM to model domain\n",
    "     rotation=127.,                  # rotate DEM around relative to Omega in °\n",
    "     )\n",
    "\n",
    "# Incorporate topography with *xyz* file\n",
    "# #############################################################################\n",
    "\n",
    "M2 = BlankWorld(                     # blank 3D domain\n",
    "     name='valley_mesh_from_xyz',    # name of the output mesh\n",
    "     m_dir='./meshes',               # main mesh directory\n",
    "     t_dir=t_dir,                    # topo dir., **p_dir**+ '/topo'\n",
    "     backup_script = False,          # store mesh generation script as backup\n",
    "     #\n",
    "     # keyword arguments regarding building the 2D surface mesh.\n",
    "     #\n",
    "     # inner_area='ellipse',         # refinement of area at surface\n",
    "     # inner_area_size=[2e3, 1e3],   # size of this area\n",
    "     # inner_area_shift=[0., 0.],    # shift in x- and y-dir of area\n",
    "     # inner_area_cell_size=1e3,     # max. triangle-size (MA) in area\n",
    "     # outer_area_cell_size=1e5,     # MA outside of this area\n",
    "\n",
    "     # keyword arguments regarding incorporation of topography\n",
    "     #\n",
    "     topo='valley_DEM.xyz',          # topography (from DEM or synthetic),\n",
    "     # # # # # # # # # # # # # # # # #                      e.g. topo=valley\n",
    "     # subsurface_topos=None,        # subsurface interface topo (DEM, synth.)\n",
    "     # easting_shift=-723000,        # shift DEM east. relative to model domain\n",
    "     # northing_shift=-5622000,      # shift DEM north. relative to model domain\n",
    "     centering=True,                 # center DEM to model domain\n",
    "     # rotation=None,                # rotate DEM realative to model domain\n",
    "     )\n",
    "\n",
    "# Incorporate topography with f(x,y) definition with a function\n",
    "# #############################################################################\n",
    "\n",
    "M3 = BlankWorld(                     # blank 3D domain\n",
    "     name='valley_mesh_from_func',   # name of the output mesh\n",
    "     m_dir='./meshes',               # main mesh directory\n",
    "     t_dir=t_dir,                    # topo dir., **p_dir**+ '/topo'\n",
    "     backup_script = False,          # store mesh generation script as backup\n",
    "     #\n",
    "     # keyword arguments regarding building the 2D surface mesh.\n",
    "     #\n",
    "     # inner_area='ellipse',         # refinement of area at surface\n",
    "     # inner_area_size=[2e3, 1e3],   # size of this area\n",
    "     # inner_area_shift=[0., 0.],    # shift in x- and y-dir of area\n",
    "     # inner_area_cell_size=1e3,     # max. triangle-size (MA) in area\n",
    "     # outer_area_cell_size=1e5,     # MA outside of this area\n",
    "\n",
    "     # keyword arguments regarding incorporation of topography\n",
    "     #\n",
    "     topo=valley,                    # topography (from DEM or synthetic),\n",
    "     # # # # # # # # # # # # # # # # #                      e.g. topo=valley\n",
    "     # subsurface_topos=None,        # subsurface interface topo (DEM, synth.)\n",
    "     # easting_shift=-723000,        # shift DEM east. relative to model domain\n",
    "     # northing_shift=-5622000,      # shift DEM north. relative to model domain\n",
    "     # centering=False,              # center DEM to model domain\n",
    "     # rotation=None,                # rotate DEM realative to model domain\n",
    "     )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create surface mesh"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Build the 2D surface mesh and incorporate transmitter or observation lines.\n",
    "# #############################################################################\n",
    "\n",
    "M1.build_surface(\n",
    "    # insert_lines=[mu.line_x(-5e3, 5e3, 1000)],  # add observation line\n",
    "    insert_loop_tx=[mu.loop_r(start=[-5e2, -5e2],\n",
    "                              stop=[5e2, 5e2], n_segs=100)],  # add Tx loop_r\n",
    "    )              \n",
    "\n",
    "M2.build_surface(\n",
    "    insert_line_tx=[mu.line_x(-1e3, 1e3, 1000)],   # add line Tx\n",
    "    )   \n",
    "M3.build_surface(\n",
    "    insert_line_tx=[mu.line_x(-1e3, 1e3, 1000)],   # add line Tx\n",
    "    )             "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Build 3D world based on 2D surface mesh\n",
    "\n",
    "- Build either a fullspace, halfspace or layered-earth mesh"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# The most simple world is a fullspace, in this case the **build_surface**\n",
    "# method does not need to be executed or is ignored, respectively.\n",
    "# #############################################################################\n",
    "\n",
    "# M1.build_fullspace_mesh()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Build a halfspace model or halfspace-like if topography is not *None*.\n",
    "# #############################################################################\n",
    "\n",
    "M1.build_halfspace_mesh()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Build a layered-earth mesh, topography can be set for subsurface layers\n",
    "# when initializing the **BlankWorld** or in this function call as keyword arg.\n",
    "# If subsurface topo is used, *layer_depths* will be ignored.\n",
    "# #############################################################################\n",
    "\n",
    "M2.build_layered_earth_mesh(n_layers=2,                   # number of layers\n",
    "                            layer_depths=[-1200.],         # n-1 layer depths\n",
    "                            )\n",
    "M3.build_layered_earth_mesh(n_layers=2,                   # number of layers\n",
    "                            layer_depths=[-1200.],         # n-1 layer depths\n",
    "                            )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Add anomalies to the subsurface\n",
    "\n",
    "- Anomalies are not allowed to intersect layers (for now)!\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Add brick anomaly\n",
    "# #############################################################################\n",
    "\n",
    "M1.add_brick(start=[-700., 300., -500.],       # lower left back corner\n",
    "             stop=[-500., 700., -1000.],      # upper right front corner\n",
    "             cell_size=1e4)                      # max. cell volume constraint"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Add dipping plate anomaly\n",
    "# #############################################################################\n",
    "\n",
    "#M2.add_plate(500., 500., 50.,                  # length, width, height\n",
    "#             [0.0, 0.0, -700.],             # origin (center) of plate\n",
    "#             45., 117.,                        # dip, dip azimuth\n",
    "#             cell_size=1e4)                    # max. cell volume constraint\n",
    "#M3.add_plate(500., 500., 50.,                  # length, width, height\n",
    "#             [0.0, 0.0, -700.],             # origin (center) of plate\n",
    "#             45., 117.,                        # dip, dip azimuth\n",
    "#             cell_size=1e4)                    # max. cell volume constraint"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Add tetrahedron-boundary\n",
    "\n",
    "- Used to increase the domain size for reducing boundary effects (low freq.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# The computational domain size is increased in x-, y- z-dir. by the \n",
    "# three given factors, respecively.\n",
    "# #############################################################################\n",
    "\n",
    "M1.extend_world(1e1, 1e1, 1e1)\n",
    "# M2.there_is_always_a_bigger_world(1e2, 1e2, 1e2)\n",
    "# M3.there_is_always_a_bigger_world(1e2, 1e2, 1e2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Call TetGen for meshing\n",
    "\n",
    "- Automatically export **Omega** to *.poly* file in **w_dir**\n",
    "- Call TetGen and export in *.vtk* for Paraview and *.mesh* (Medit format)\n",
    "- Automatically copy *.mesh* filed to **s_dir** for automated conversion"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Call TetGen with different command line options and set further arguments.\n",
    "# for the TetGen options, it is referred to the TetGen documentation\n",
    "# #############################################################################\n",
    "\n",
    "M1.call_tetgen(tet_param='-pq1.2aA',\n",
    "              # tet_param='default,             # '-pq1.2aA',\n",
    "              # tet_param='raw',                # '-p'\n",
    "              # tet_param='faces',              # '-pMA'\n",
    "              export_vtk=True                   # export *.vtk* for Paraview\n",
    "              # print_infos=True,               # print mesh infos at the end\n",
    "              # export_before=True,             # export *.poly* file\n",
    "              # # # # # # # # # # # # # # # # # # automatically\n",
    "              # copy=True                       # copy to **s_dir**\n",
    "              ) \n",
    "\n",
    "M2.call_tetgen(export_vtk=True)\n",
    "M3.call_tetgen(export_vtk=True)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
