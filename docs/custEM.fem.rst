custEM.fem package
==================

Submodules
----------

custEM.fem.assembly module
--------------------------

.. automodule:: custEM.fem.assembly
   :members:
   :undoc-members:
   :show-inheritance:

custEM.fem.calc\_primary\_boundary\_fields module
-------------------------------------------------

.. automodule:: custEM.fem.calc_primary_boundary_fields
   :members:
   :undoc-members:
   :show-inheritance:

custEM.fem.calc\_primary\_fields module
---------------------------------------

.. automodule:: custEM.fem.calc_primary_fields
   :members:
   :undoc-members:
   :show-inheritance:

custEM.fem.fem\_base module
---------------------------

.. automodule:: custEM.fem.fem_base
   :members:
   :undoc-members:
   :show-inheritance:

custEM.fem.fem\_utils module
----------------------------

.. automodule:: custEM.fem.fem_utils
   :members:
   :undoc-members:
   :show-inheritance:

custEM.fem.frequency\_domain\_approaches module
-----------------------------------------------

.. automodule:: custEM.fem.frequency_domain_approaches
   :members:
   :undoc-members:
   :show-inheritance:

custEM.fem.primary\_fields module
---------------------------------

.. automodule:: custEM.fem.primary_fields
   :members:
   :undoc-members:
   :show-inheritance:

custEM.fem.time\_domain\_approaches module
------------------------------------------

.. automodule:: custEM.fem.time_domain_approaches
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: custEM.fem
   :members:
   :undoc-members:
   :show-inheritance:
