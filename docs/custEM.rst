custEM package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   custEM.core
   custEM.fem
   custEM.inv
   custEM.meshgen
   custEM.misc
   custEM.post

Module contents
---------------

.. automodule:: custEM
   :members:
   :undoc-members:
   :show-inheritance:
